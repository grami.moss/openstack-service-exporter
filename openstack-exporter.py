#!/usr/bin/env python3
"""Application exporter"""

import os
import dill
import logging
import argparse
import hashlib
import openstack
import json
import collections
from netaddr import IPRange
from threading import Thread
from threading import RLock
from collections import UserDict
from prometheus_client import (
    Gauge,
    CollectorRegistry,
    generate_latest,
    CONTENT_TYPE_LATEST,
)
from time import sleep, time
from wsgiref.simple_server import make_server
from cachetools import LRUCache
from functools import wraps

collectors = []
log = logging.getLogger("poe-logger")

valid_statuses = [
    "ACTIVE",
    "ERROR",
    "SHELVED_OFFLOADED",
    "SHUTOFF",
    "SUSPENDED",
    "VERIFY_RESIZE",
]


# Define a custom decorator for LRU caching
def lru_cache_decorator(cache):
    def decorator(func):
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            # Check if the result is already cached
            if args in cache:
                log.debug(f"Fetching {func.__name__} from cache...")
                return cache[args]

            # If not cached, compute the result and cache it
            log.debug(f"Calculating {func.__name__}...")
            result = func(self, *args, **kwargs)
            cache[args] = result  # Cache the result
            return result

        return wrapper

    return decorator


# Helper function to compute a hash of the data
def compute_data_hash(data):
    return hashlib.md5(str(data).encode("utf-8")).hexdigest()


# Decorator to handle pickling only if the data has changed
def pickle_on_return(func):
    def wrapper(self, *args, **kwargs):
        result = func(
            self, *args, **kwargs
        )  # Call the original function to get the result

        # Only pickle if the data has changed
        if result:
            current_hash = compute_data_hash(result)  # Compute the hash of the new data

            # Check if the pickle file exists and has a stored hash
            if os.path.exists(self._file):
                with open(self._file, "rb") as wr_file:
                    # Load the previous data and compare its hash
                    previous_data = dill.load(wr_file)
                    previous_hash = compute_data_hash(previous_data)
                    if current_hash != previous_hash:  # Data has changed
                        log.debug("Data has changed, writing to pickle")
                        with open(self._file, "wb") as wr_file:
                            dill.dump(
                                result, wr_file
                            )  # Save the new data to the pickle file

            else:
                # If the pickle file doesn't exist, write the new data
                log.debug("No pickle file found, creating new one")
                with open(self._file, "wb") as wr_file:
                    dill.dump(result, wr_file)  # Save the result to the pickle file

        return result  # Return the original result

    return wrapper


class ThreadSafeDict(UserDict):
    def __init__(self, *p_arg, **n_arg):
        super().__init__(*p_arg, **n_arg)
        self._lock = RLock()

    def __setitem__(self, key, value):
        with self._lock:
            self.data[key] = value

    def __delitem__(self, key):
        with self._lock:
            del self.data[key]

    def pop(self, key, default=None):
        with self._lock:
            return self.data.pop(key, default)

    def update(self, other=None, **kwargs):
        with self._lock:
            self.data.update(other, **kwargs)

    def __getitem__(self, key):
        with self._lock:
            return self.data[key]

    def copy(self):
        with self._lock:
            return self.data.copy()

    def __len__(self):
        with self._lock:
            return len(self.data)

    def __enter__(self):
        self._lock.acquire()
        return self

    def __exit__(self, type, value, traceback):
        self._lock.release()

    def __getstate__(self):
        state = self.__dict__.copy()
        # If _body is the problematic attribute, you can choose not to pickle it
        state.pop("_body", None)  # Remove _body from the state if it's not needed
        return state

    def __setstate__(self, state):
        # Restore the object's state during unpickling
        self.__dict__.update(state)
        # Reinitialize _body if necessary
        self._body = None  # or some default value if needed


class collectorBase:
    def __init__(self, cloud, cache):
        self.cloud = cloud
        self.cache = cache
        self._file = f"/tmp/{self.get_cache_key()}.pickle"
        self.RESOURCE_PROVIDER_AGGREGATE_CACHE = {}

    @property
    @pickle_on_return  # Apply the decorator to the 'data' property
    def data(self):
        d = self.cache.get_cache_data(self.get_cache_key())
        if d:
            return d

    def get_cache_key(self):
        return self.__class__.__name__

    def gen_data(self):
        data = {}
        return data

    def get_stats(self):
        registry = CollectorRegistry()
        return generate_latest(registry)

    def get_project_name(self, id):
        projectData = self.cloud.identity.find_project(id)
        if projectData:
            return projectData["name"]
        else:
            return None

    def _get_keystone_info(self):
        """Return a dict containing tentants details."""
        info = {}
        try:
            info["tenants"] = {
                x.id: {"name": x.name} for x in self.cloud.identity.projects()
            }
        except AttributeError:
            log.info("Error projects.list")
            log.debug("Number of projects: %s", len(info["tenants"]))
        return info

    def _get_hypervisors_info(self):
        info = {}
        try:
            info["hypervisors"] = {
                x.id: {
                    "hypervisor_type": x.hypervisor_type,
                    "hypervisor_version": x.hypervisor_version,
                    "name": x.name,
                    "state": x["state"],
                    "status": x["status"],
                    "host_ip": x["host_ip"],
                    "cpu_info": x["cpu_info"],
                }
                for x in self.cloud.compute.hypervisors()
            }
        except AttributeError as e:
            log.info("Error getting hypervisors")
            log.error(e)
            log.debug("Number of hypervisors: %s", len(info["hypervisors"]))
        return info

    def _get_network_info(self):
        """Return a dict containing network details."""
        info = {}
        try:
            # "agent_type", "binary", "host", "admin_state_up", "availability_zone" , "alive"
            info["agents"] = [
                {
                    "id": x.id,
                    "agent_type": x.agent_type,
                    "binary": x.binary,
                    "host": x.host,
                    "admin_state_up": x["admin_state_up"],
                    "availability_zone": x["availability_zone"],
                    "alive": x["alive"],
                }
                for x in self.cloud.network.agents()
            ]
            info["routers"] = {
                x.id: {
                    "name": x.name,
                    "project_id": x.project_id,
                    "external_gateway_info": {
                        "network_id": None
                        if x.external_gateway_info is None
                        else x.external_gateway_info["network_id"]
                    },
                    "status": x["status"],
                    "admin_state_up": x["admin_state_up"],
                }
                for x in self.cloud.network.routers()
            }

            info["networks"] = {
                x.id: {
                    "name": x.name,
                    "subnets": x.subnet_ids if x.subnet_ids else [],
                }
                for x in self.cloud.network.networks()
            }

            info["ports"] = {
                x.id: {
                    "device_id": x.device_id,
                    "device_owner": x.device_owner,
                    "status": x.status,
                    "fixed_ips": x.fixed_ips,
                }
                for x in self.cloud.network.ports()
            }

            info["subnets"] = {
                x["id"]: {"name": x["name"], "pool": x["allocation_pools"]}
                for x in self.cloud.network.subnets()
            }
            info["floatingips"] = [
                {
                    "id": x.id,
                    "name": x.name,
                    "status": x.status,
                    "floating_network_id": x.floating_network_id,
                    "tenant_id": x.tenant_id,
                }
                for x in self.cloud.network.ips()
            ]

        except AttributeError as e:
            log.info("Error getting networks")
            log.error(e)
            log.debug("Number of agents: %s", len(info["agents"]))
            log.debug("Number of routers: %s", len(info["routers"]))
            log.debug("Number of networks: %s", len(info["networks"]))
        return info

    def get_resource_provider_info(self):
        # get host aggregates to look up things like az
        nova_aggregates = list(self.cloud.compute.aggregates())

        azones = {}
        project_filters = {}
        for agg in nova_aggregates:
            az = agg.metadata.get("availability_zone")
            if az:
                azones[agg.uuid] = az

            projects = []
            for key in agg.metadata.keys():
                if key.startswith("filter_tenant_id"):
                    projects.append(agg.metadata[key])
            if projects:
                # TODO(johngarbutt): expose project id to aggregate names?
                project_filters[agg.uuid] = {"name": agg.name, "projects": projects}

        raw_rps = list(self.cloud.placement.resource_providers())

        resource_providers = {}
        for raw_rp in raw_rps:
            rp = {"uuid": raw_rp.id}

            # Use parent id if resource provider is a PCI device
            if raw_rp.parent_provider_id is not None:
                rp_id = raw_rp.parent_provider_id
            else:
                rp_id = raw_rp.id

            # TODO(johngarbutt): maybe check if cached aggregate still exists?
            aggregates = self.RESOURCE_PROVIDER_AGGREGATE_CACHE.get(rp_id)
            if aggregates is None:
                response = self.cloud.placement.get(
                    f"/resource_providers/{rp_id}/aggregates",
                    headers={"OpenStack-API-Version": "placement 1.39"},
                )
                response.raise_for_status()
                aggs = response.json()
                rp["aggregates"] = aggs["aggregates"]
                self.RESOURCE_PROVIDER_AGGREGATE_CACHE[rp_id] = aggs["aggregates"]
            else:
                rp["aggregates"] = aggregates

            for agg_id in rp["aggregates"]:
                if agg_id in azones:
                    rp["az"] = azones[agg_id]
                if agg_id in project_filters:
                    # TODO(johngarbutt): loosing info here
                    if "project_filter" in rp:
                        rp["project_filter"] = "multiple"
                    else:
                        rp["project_filter"] = project_filters[agg_id]["name"]
            resource_providers[raw_rp.name] = rp
        project_to_aggregate = collections.defaultdict(list)
        for filter_info in project_filters.values():
            name = filter_info["name"]
            for project in filter_info["projects"]:
                project_to_aggregate[project] += [name]

        return resource_providers, project_to_aggregate


class octavia(collectorBase):
    def gen_data(self):
        data = {
            "loadbalancers": {},
            "pools": {},
        }
        pools = self.cloud.load_balancer.pools()
        for pool in pools:
            data["pools"][pool["id"]] = {
                "name": pool["name"],
                "project": self.get_project_name(pool["project_id"]),
                "provisioning_status": pool["provisioning_status"],
                "operating_status": pool["operating_status"],
            }
        loadbalancers = self.cloud.load_balancer.load_balancers()
        for loadbalancer in loadbalancers:
            data["loadbalancers"][loadbalancer["id"]] = {
                "name": loadbalancer["name"],
                "project": self.get_project_name(loadbalancer["project_id"]),
                "provisioning_status": loadbalancer["provisioning_status"],
                "operating_status": loadbalancer["operating_status"],
                "vip_address": loadbalancer["vip_address"],
            }
        return data

    def get_stats(self):
        registry = CollectorRegistry()
        guageLoadbalancers = Gauge(
            "openstack_exporter_loadbalancers",
            "list of loadbalancers",
            [
                "id",
                "name",
                "project",
                "provisioning_status",
                "operating_status",
                "vip_address",
            ],
            registry=registry,
        )
        guagepool = Gauge(
            "openstack_exporter_pools",
            "list of pools",
            ["id", "name", "project", "provisioning_status", "operating_status"],
            registry=registry,
        )
        data = self.data
        if data:
            for lb in data["loadbalancers"]:
                guageLoadbalancers.labels(
                    lb,
                    data["loadbalancers"][lb]["name"],
                    data["loadbalancers"][lb]["project"],
                    data["loadbalancers"][lb]["provisioning_status"],
                    data["loadbalancers"][lb]["operating_status"],
                    data["loadbalancers"][lb]["vip_address"],
                ).inc()
            for pool in data["pools"]:
                guagepool.labels(
                    lb,
                    data["pools"][pool]["name"],
                    data["pools"][pool]["project"],
                    data["pools"][pool]["provisioning_status"],
                    data["pools"][pool]["operating_status"],
                ).inc()
        return generate_latest(registry)


class glance(collectorBase):
    def gen_data(self):
        data = {}
        for i in self.cloud.image.images():
            servers = self.cloud.compute.servers(all_projects=True, image=i["id"])
            count = 0
            for s in servers:
                count += 1
            data[i["name"]] = {"id": i["id"], "count": count}
        return data

    def get_stats(self):
        registry = CollectorRegistry()
        data = self.data
        gaugeImageCount = Gauge(
            "openstack_exporter_image_count",
            "than scraping interval so we use Gauge",
            ["image_id", "image"],
            registry=registry,
        )
        for image in data:
            gaugeImageCount.labels(data[image]["id"], image).set(data[image]["count"])
        return generate_latest(registry)


class instances(collectorBase):
    def gen_data(self):
        data = {"flavors": {}, "instances": {}}
        for server in self.cloud.compute.servers(all_projects=True):
            projectData = self.cloud.identity.find_project(server["project_id"])
            if projectData:
                ProjectName = projectData["name"]
            else:
                ProjectName = "orphaned"

            data["instances"][server["id"]] = {
                "name": server["name"],
                "status": server["status"],
                "compute_host": server["compute_host"],
                "availability_zone": server["availability_zone"],
                "project_id": server["project_id"],
                "project": ProjectName,
                "power_state": server["power_state"],
                "image": server["image"]["id"],
                "flavor": server["flavor"]["original_name"],
            }
            if server["flavor"]["original_name"] in data["flavors"]:
                data["flavors"][server["flavor"]["original_name"]] += 1
            else:
                data["flavors"][server["flavor"]["original_name"]] = 1
        return data

    def get_stats(self):
        registry = CollectorRegistry()
        data = self.data
        gaugeFlavors = Gauge(
            "openstack_exporter_flavor_count",
            "than scraping interval so we use Gauge",
            ["flavor"],
            registry=registry,
        )
        instances = Gauge(
            "nova_instances",
            "Nova instances metrics",
            [
                "name",
                "tenant",
                "tenant_id",
                "instance_state",
                "instance_id",
                "compute_host",
                "flavor",
                "image_id",
            ],
            registry=registry,
        )
        if data:
            for i in data["instances"]:
                instances.labels(
                    data["instances"][i]["name"],
                    data["instances"][i]["project"],
                    data["instances"][i]["project_id"],
                    data["instances"][i]["status"],
                    i,
                    data["instances"][i]["compute_host"],
                    data["instances"][i]["flavor"],
                    data["instances"][i]["image"],
                ).set(data["instances"][i]["power_state"])
            for flavor in data["flavors"]:
                gaugeFlavors.labels(flavor).set(data["flavors"][flavor])

        return generate_latest(registry)


class nova(collectorBase):
    def __init__(self, *args, **kwargs):
        super(nova, self).__init__(*args, **kwargs)
        self.labels = ["tenant", "tenant_id", "type", "domain"]

    def get_stats(self):
        registry = CollectorRegistry()
        nova_quota_cores = Gauge(
            "nova_quota_cores",
            "Nova cores metric",
            self.labels,
            registry=registry,
        )
        nova_quota_instances = Gauge(
            "nova_quota_instances",
            "Nova instances (number)",
            self.labels,
            registry=registry,
        )
        nova_quota_ram_mbs = Gauge(
            "nova_quota_ram_mbs",
            "Nova RAM (MB)",
            self.labels,
            registry=registry,
        )
        nova_hypervisors = Gauge(
            "nova_hypervisors",
            "Nova hypervisors state",
            [
                "hypervisor_name",
                "hypervisor_id",
                "hypervisor_type",
                "hypervisor_version",
                "status",
            ],
            registry=registry,
        )
        project_data = self.data["projects"]
        for project in project_data:
            for quota in project_data[project]["quotas"]:
                for quotaType in project_data[project]["quotas"][quota]:
                    label_value = [
                        project,
                        project_data[project]["id"],
                        quotaType,
                        project_data[project]["domain"],
                    ]
                    if quota == "nova_quota_cores":
                        nova_quota_cores.labels(*label_value).set(
                            project_data[project]["quotas"][quota][quotaType]
                        )
                    if quota == "nova_quota_instances":
                        nova_quota_instances.labels(*label_value).set(
                            project_data[project]["quotas"][quota][quotaType]
                        )
                    if quota == "nova_quota_ram_mbs":
                        nova_quota_ram_mbs.labels(*label_value).set(
                            project_data[project]["quotas"][quota][quotaType]
                        )

        hypervisors = self.data["hypervisors"]
        for hyp_id, hyp_data in hypervisors.items():
            amount = True if hyp_data["state"] == "up" else False
            nova_hypervisors.labels(
                hyp_data["name"],
                hyp_id,
                hyp_data["hypervisor_type"],
                hyp_data["hypervisor_version"],
                hyp_data["status"],
            ).set(amount)
        return generate_latest(registry)

    def gen_data(self):
        data = self._get_hypervisors_info()
        data["projects"] = {}
        for project in self.cloud.identity.projects():
            quota = self.cloud.get_compute_limits(project["id"])
            domain = self.cloud.identity.get_domain(project["domain_id"])
            data["projects"][project["name"]] = {
                "id": project["id"],
                "domain": domain["name"],
                "quotas": {
                    "nova_quota_cores": {
                        "limit": quota["max_total_cores"],
                        "in_use": quota["total_cores_used"],
                    },
                    "nova_quota_instances": {
                        "limit": quota["max_total_instances"],
                        "in_use": quota["total_instances_used"],
                    },
                    "nova_quota_ram_mbs": {
                        "limit": quota["max_total_ram_size"],
                        "in_use": quota["total_ram_used"],
                    },
                },
            }
        return data


class placement(collectorBase):
    def __init__(self, *args, **kwargs):
        super(placement, self).__init__(*args, **kwargs)
        self.labels = [
            "hypervisor_hostname",
            "aggregate",
            "availability_zone",
            "nova_service_status",
        ]

    def get_stats(self):
        registry = CollectorRegistry()
        vms = Gauge(
            "hypervisor_running_vms",
            "Number of running VMs",
            self.labels,
            registry=registry,
        )
        vcpus_exactly = Gauge(
            "hypervisor_vcpus_exact",
            "exact number of CPUs",
            self.labels,
            registry=registry,
        )
        vcpus_total = Gauge(
            "hypervisor_vcpus_total",
            "Total number of vCPUs",
            self.labels,
            registry=registry,
        )
        vcpus_used = Gauge(
            "hypervisor_vcpus_used",
            "Number of used vCPUs",
            self.labels,
            registry=registry,
        )
        mem_total = Gauge(
            "hypervisor_memory_mbs_total",
            "Total amount of memory in MBs",
            self.labels,
            registry=registry,
        )
        mem_used = Gauge(
            "hypervisor_memory_mbs_used",
            "Used memory in MBs",
            self.labels,
            registry=registry,
        )
        disk_total = Gauge(
            "hypervisor_disk_gbs_total",
            "Total amount of disk space in GBs",
            self.labels,
            registry=registry,
        )
        disk_used = Gauge(
            "hypervisor_disk_gbs_used",
            "Used disk space in GBs",
            self.labels,
            registry=registry,
        )

        data = self.data
        for host in data:
            if data[host]["nova_service_status"]:
                label_value = [
                    host,
                    data[host]["aggregate"],
                    data[host]["availability_zone"],
                    data[host]["nova_service_status"],
                ]
                cpu_type = "VCPU" if "VCPU" in data[host]["totals"] else "PCPU"
                vms.labels(*label_value).set(data[host]["vms"])
                vcpus_total.labels(*label_value).set(data[host]["totals"][cpu_type])
                vcpus_exactly.labels(*label_value).set(data[host]["exact"])
                mem_total.labels(*label_value).set(data[host]["totals"]["MEMORY_MB"])
                disk_total.labels(*label_value).set(data[host]["totals"]["DISK_GB"])

                vcpus_used.labels(*label_value).set(data[host]["usages"][cpu_type])
                mem_used.labels(*label_value).set(data[host]["usages"]["MEMORY_MB"])
                disk_used.labels(*label_value).set(data[host]["usages"]["DISK_GB"])
        return generate_latest(registry)

    def find_availability_zone(self, host):
        if host is None or host == "":
            return None
        for availability_zone in self.cloud.compute.availability_zones(details=True):
            if host.lower() in availability_zone.hosts:
                return availability_zone.name
        return None

    def find_agg(self, host):
        for aggregates in self.cloud.compute.aggregates():
            if (not aggregates["availability_zone"]) or (
                "nova" in aggregates["availability_zone"]
            ):
                if host in aggregates["hosts"]:
                    return aggregates["name"]
        return None

    def gen_data(self):
        resource_providers = self.cloud.placement.resource_providers()
        host_data = {}
        for resource_provider in resource_providers:
            links = {}
            for link in resource_provider["links"]:
                links[link["rel"]] = link["href"]
            inventories = self.cloud.placement.get(links["inventories"]).json()[
                "inventories"
            ]
            usages = self.cloud.placement.get(links["usages"]).json()["usages"]
            inv = {}
            exact = 0
            for item in inventories:
                log.debug(f"placement: item :: {item}")
                if "total" in inventories[item]:
                    inv[item] = (
                        inventories[item]["total"]
                        * inventories[item]["allocation_ratio"]
                    )
                if ("VCPU" in item) or ("PCPU" in item):
                    exact = inventories[item]["total"]
            log.debug(f"placement: resource_provider :: {resource_provider.name}")
            services = list(self.cloud.compute.services(host=resource_provider.name))
            service = services[0] if len(services) > 0 else {}
            log.debug(f"placement: service :: {service}")

            host_data[resource_provider.name] = {}
            host_data[resource_provider.name]["vms"] = len(
                self.cloud.placement.get(links["allocations"]).json()["allocations"]
            )
            host_data[resource_provider.name]["totals"] = inv
            host_data[resource_provider.name]["exact"] = exact
            host_data[resource_provider.name]["usages"] = usages
            host_data[resource_provider.name]["aggregate"] = self.find_agg(
                resource_provider.name
            )
            host_data[resource_provider.name]["availability_zone"] = (
                service["availability_zone"] if "availability_zone" in service else None
            )
            host_data[resource_provider.name]["nova_service_status"] = (
                service["status"] if "status" in service else None
            )
        return host_data


class capacity_project(collectorBase):
    def gen_data(self):
        projects = {
            proj.id: dict(name=proj.name, usages={}, quotas={})
            for proj in self.cloud.identity.projects()
        }
        for project_id in projects.keys():
            response = self.cloud.placement.get(
                f"/usages?project_id={project_id}",
                headers={"OpenStack-API-Version": "placement 1.39"},
            )
            response.raise_for_status()
            usages = response.json()
            projects[project_id]["usages"] = usages["usages"]
            for usage_type in projects[project_id]["usages"]:
                projects[project_id]["usages"][usage_type].pop("consumer_count")
            block_quota = self.cloud.block_storage.get_quota_set(project_id, usage=True)
            compute_quota = self.cloud.compute.get_quota_set(project_id, usage=True)

            # log.debug(json.dumps(projects[project_id]["usages"], indent=2))
            projects[project_id]["usages"]["quota"] = dict(
                VOLUMES=block_quota["usage"].get("volumes"),
                GIGABYTES=block_quota["usage"].get("gigabytes"),
            )
            projects[project_id]["quotas"].update(
                dict(
                    INSTANCES=compute_quota.get("instances"),
                    CPUS=compute_quota.get("cores"),
                    MEMORY_MB=compute_quota.get("ram"),
                    VOLUMES=block_quota.get("volumes"),
                    GIGABYTES=block_quota.get("gigabytes"),
                )
            )
        log.debug(json.dumps(projects, indent=2))
        return projects

    def get_stats(self):
        registry = CollectorRegistry()
        projects = self.data
        project_usage_guage = Gauge(
            "openstack_project_usage",
            "Current placement allocations per project.",
            ["project_id", "project_name", "placement_resource"],
            registry=registry,
        )
        project_quota_guage = Gauge(
            "openstack_project_quota",
            "Current quota set to limit max resource allocations per project.",
            ["project_id", "project_name", "quota_resource"],
            registry=registry,
        )
        for project_id, data in projects.items():
            name = data["name"]
            project_usages = data["usages"]
            for resource, amount in project_usages.items():
                for resource, value in amount.items():
                    project_usage_guage.labels(project_id, name, resource).set(value)

            if not project_usages:
                # skip projects with zero usage?
                log.warning(f"WARNING no usage for project: {name} {project_id}")
            project_quotas = data["quotas"]
            for resource, amount in project_quotas.items():
                project_quota_guage.labels(project_id, name, resource).set(amount)
        return generate_latest(registry)


class capacity_resources(collectorBase):
    def gen_data(self):
        resource_providers, project_to_aggregate = self.get_resource_provider_info()
        for name, data in resource_providers.items():
            rp_id = data["uuid"]
            response = self.cloud.placement.get(
                f"/resource_providers/{rp_id}/usages",
                headers={"OpenStack-API-Version": "placement 1.19"},
            )
            response.raise_for_status()
            rp_usages = response.json()["usages"]
            resource_providers[name]["usages"] = rp_usages

            response = self.cloud.placement.get(
                f"/resource_providers/{rp_id}/inventories",
                headers={"OpenStack-API-Version": "placement 1.19"},
            )
            response.raise_for_status()
            inventories = response.json()["inventories"]
            resource_providers[name]["inventories"] = inventories
        return resource_providers

    def get_stats(self):
        registry = CollectorRegistry()
        resource_providers = self.data
        usage_guage = Gauge(
            "openstack_hypervisor_placement_allocated",
            "Currently allocated resource for each provider in placement.",
            ["hypervisor", "resource"],
            registry=registry,
        )
        capacity_guage = Gauge(
            "openstack_hypervisor_placement_allocatable_capacity",
            "The total allocatable resource in the placement inventory.",
            ["hypervisor", "resource"],
            registry=registry,
        )
        for name, data in resource_providers.items():
            rp_usages = resource_providers[name]["usages"]
            for resource, amount in rp_usages.items():
                usage_guage.labels(name, resource).set(amount)
            inventories = resource_providers[name]["inventories"]
            for resource, data in inventories.items():
                amount = (
                    int(data["total"] * data["allocation_ratio"]) - data["reserved"]
                )
                capacity_guage.labels(name, resource).set(amount)
        return generate_latest(registry)


class capacity_host(collectorBase):
    def get_capacity_per_flavor(self, flavors):
        capacity_per_flavor = {}
        for flavor in flavors:
            max_per_host = self.get_max_per_host(flavor)
            capacity_per_flavor[flavor.name] = max_per_host

        return capacity_per_flavor

    def get_placement_request(self, flavor):
        resources = {}
        required_traits = []

        def add_defaults(resources, flavor, skip_vcpu=False):
            resources["MEMORY_MB"] = flavor.ram
            resources["DISK_GB"] = flavor.disk + flavor.ephemeral
            if not skip_vcpu and "PCPU" not in resources:
                resources["VCPU"] = flavor.vcpus

        for key, value in flavor.extra_specs.items():
            if "trait:" == key[:6]:
                if value == "required":
                    required_traits.append(key[6:])
            if "resources:" == key[:10]:
                count = int(value)
                resources[key[10:]] = count
            if "hw:cpu_policy" == key and value == "dedicated":
                resources["PCPU"] = flavor.vcpus
                add_defaults(resources, flavor, skip_vcpu=True)

        # if not baremetal and not PCPU
        # we should add the default vcpu ones
        if not resources or "VGPU" in resources:
            add_defaults(resources, flavor)

        return resources, required_traits

    def get_max_per_host(self, flavor):
        resources, required_traits = self.get_placement_request(flavor)
        resource_str = ",".join(
            [key + ":" + str(value) for key, value in resources.items() if value]
        )

        required_traits.append("!COMPUTE_STATUS_DISABLED")
        required_str = ",".join(required_traits)
        params = {"resources": resource_str}

        if not resource_str:
            raise Exception("we must have some resources here!")
        if required_str:
            params["required"] = required_str
        response = self.cloud.placement.get(
            "/allocation_candidates",
            params=params,
            headers={"OpenStack-API-Version": "placement 1.39"},
        )

        raw_data = response.json()
        count_per_rp = {}

        merged_resource = {}
        for rp_uuid, summary in raw_data.get("provider_summaries", {}).items():
            summary = json.loads(json.dumps(summary))
            if summary["parent_provider_uuid"] is None:
                merged_resource[rp_uuid] = summary
            # Think I'm missing some of the hosts need to double check this.
            elif summary["parent_provider_uuid"] and any(
                elem in required_traits for elem in summary["traits"]
            ):
                if summary["parent_provider_uuid"] in merged_resource:
                    for resource in summary["resources"]:
                        if (
                            resource
                            not in merged_resource[summary["parent_provider_uuid"]][
                                "resources"
                            ]
                        ):
                            merged_resource[summary["parent_provider_uuid"]][
                                "resources"
                            ][resource] = summary["resources"][resource]
                        else:
                            merged_resource[summary["parent_provider_uuid"]][
                                "resources"
                            ][resource]["capacity"] += summary["resources"][resource][
                                "capacity"
                            ]
                            merged_resource[summary["parent_provider_uuid"]][
                                "resources"
                            ][resource]["used"] += summary["resources"][resource][
                                "used"
                            ]

        for rp_uuid, summary in merged_resource.items():
            # per resource, get max possible number of instances
            max_counts = []
            for resource, amounts in summary["resources"].items():
                requested = resources.get(resource, 0)
                if requested:
                    free = amounts["capacity"] - amounts["used"]
                    amount = int(free / requested)
                    max_counts.append(amount)
                    log.debug(f"{flavor.name}-{resource} = {amount}")
                # available count is the min of the max counts
            if max_counts:
                count_per_rp[rp_uuid] = min(max_counts)
        if not count_per_rp:
            log.info(
                f"# WARNING - no candidates hosts for flavor: {flavor.name} {params}"
            )
        return count_per_rp

    def gen_data(self):
        flavors = list(self.cloud.compute.flavors(get_extra_specs=True))
        capacity_per_flavor = self.get_capacity_per_flavor(flavors)
        resource_providers, project_to_aggregate = self.get_resource_provider_info()

        data = {
            "flavors": json.loads(json.dumps(flavors)),
            "capacity_per_flavor": capacity_per_flavor,
            "resource_providers": resource_providers,
            "project_to_aggregate": project_to_aggregate,
        }
        return data

    def get_stats(self):
        registry = CollectorRegistry()
        data = self.data

        # total capacity per flavor
        free_by_flavor_total = Gauge(
            "openstack_free_capacity_by_flavor_total",
            "Free capacity if you fill the cloud full of each flavor",
            ["flavor_name"],
            registry=registry,
        )

        # capacity per host
        free_by_flavor_hypervisor = Gauge(
            "openstack_free_capacity_hypervisor_by_flavor",
            "Free capacity for each hypervisor if you fill remaining space full of each flavor",
            ["hypervisor", "flavor_name", "az_aggregate", "project_aggregate"],
            registry=registry,
        )

        flavor_names = sorted([f["name"] for f in data["flavors"]])
        capacity_per_flavor = data["capacity_per_flavor"]
        for flavor_name in flavor_names:
            counts = capacity_per_flavor.get(flavor_name, {}).values()
            total = 0 if not counts else sum(counts)
            free_by_flavor_total.labels(flavor_name).set(total)

        resource_providers = data["resource_providers"]
        hostnames = sorted(resource_providers.keys())
        for hostname in hostnames:
            rp = resource_providers[hostname]
            rp_id = rp["uuid"]
            free_space_found = False
            for flavor_name in flavor_names:
                all_counts = capacity_per_flavor.get(flavor_name, {})
                our_count = all_counts.get(rp_id, 0)
                if our_count == 0:
                    continue
                az = rp.get("az", "")
                project_filter = rp.get("project_filter", "")
                free_by_flavor_hypervisor.labels(
                    hostname, flavor_name, az, project_filter
                ).set(our_count)
                free_space_found = True
            if not free_space_found:
                # TODO(johngarbutt) allocation candidates only returns some not all candidates!
                log.warning(f"# WARNING - no free spaces found for {hostname}")

        return generate_latest(registry)


class Ironic(collectorBase):
    def gen_data(self):
        data = {"hosts": {}, "projects": {}, "instances": {}}
        for host in self.cloud.baremetal.nodes():
            log.debug(f"Ironic: host :: {host}")
            if host["id"] not in data["hosts"]:
                data["hosts"][host["id"]] = {
                    "name": host["name"],
                    "instance_id": host["instance_uuid"],
                    "maintenance": host["maintenance"],
                    "power_state": str(host["power_state"]).replace("power ", ""),
                    "provision_state": host["provision_state"],
                    "project_id": None,
                }
            if host["instance_uuid"] is not None:
                instance = self.cloud.compute.find_server(
                    host["instance_uuid"], all_projects=True
                )
                if instance["id"] not in data["instances"]:
                    data["instances"][instance["id"]] = {"name": instance["name"]}

                project = self.cloud.identity.find_project(instance["project_id"])
                data["hosts"][host["id"]]["project_id"] = project["id"]
                if project["id"] not in data["projects"]:
                    data["projects"][project["id"]] = {"name": project["name"]}

        return data

    def get_stats(self):
        registry = CollectorRegistry()
        guageBaremetalNodes = Gauge(
            "Baremetal_nodes",
            "list of baremetal nodes",
            [
                "id",
                "name",
                "project",
                "maintenance",
                "provision_state",
                "power_state",
                "instance_id",
                "instance_name",
            ],
            registry=registry,
        )
        data = self.data
        if data:
            for host in data["hosts"]:
                instance_name = (
                    data["instances"][data["hosts"][host]["instance_id"]]["name"]
                    if data["hosts"][host]["instance_id"] in data["instances"]
                    else None
                )
                project = (
                    data["projects"][data["hosts"][host]["project_id"]]["name"]
                    if data["hosts"][host]["project_id"] in data["projects"]
                    else None
                )
                amount = 1 if data["hosts"][host]["power_state"] == "on" else 0
                guageBaremetalNodes.labels(
                    host,
                    data["hosts"][host]["name"],
                    project,
                    data["hosts"][host]["maintenance"],
                    data["hosts"][host]["provision_state"],
                    data["hosts"][host]["power_state"],
                    data["hosts"][host]["instance_id"],
                    instance_name,
                ).set(amount)
        return generate_latest(registry)


class Neutron(collectorBase):
    _cache = LRUCache(maxsize=500)

    @lru_cache_decorator(cache=_cache)
    def tenant_map(self, tenant_id):
        return self.data["tenants"][tenant_id]["name"]

    @lru_cache_decorator(cache=_cache)
    def network_map(self, network_id):
        return self.data["networks"][network_id]["name"]

    @lru_cache_decorator(cache=_cache)
    def subnet_map(self, subnet_id):
        return self.data["subnets"][subnet_id]["name"]

    def _get_router_ip(self, uuid):
        owner = "network:router_gateway"
        for id, port in self.data["ports"].items():
            if port["device_id"] == uuid and port["device_owner"] == owner:
                if port["status"] == "ACTIVE" and port["fixed_ips"]:
                    return port["fixed_ips"][0]["ip_address"]

    def get_router_ips(self, data):
        """Collect Router IPs."""
        ips = {}
        for id, r in data["routers"].items():
            if self._get_router_ip(id):
                try:
                    tenant = self.tenant_map(r["project_id"])
                except KeyError:
                    tenant = "Unknown tenant ({})".format(r["project_id"])
                subnet = self.network_map(r["external_gateway_info"]["network_id"])
                key = (
                    subnet,
                    tenant,
                    r["project_id"],
                    "routerip",
                    r["status"],
                )
                if key in ips:
                    ips[key] += 1
                else:
                    ips[key] = 1
        return ips

    def get_floating_ips(self, data):
        """Collect Floating IPs."""
        ips = {}
        for ip in data["floatingips"]:
            subnet = self.network_map(ip["floating_network_id"])
            try:
                tenant = self.tenant_map(ip["tenant_id"])
            except KeyError:
                tenant = "Unknown tenant ({})".format(ip["tenant_id"])
            key = (
                subnet,
                tenant,
                ip["tenant_id"],
                "floatingip",
                ip["status"],
            )
            if key in ips:
                ips[key] += 1
            else:
                ips[key] = 1
        return ips

    def gen_data(self):
        data = {}
        data = self._get_network_info()
        data["tenants"] = self._get_keystone_info()["tenants"]
        return data

    def get_stats(self):
        registry = CollectorRegistry()
        guageNeutronAgents = Gauge(
            "neutron_agents",
            "list of neutron agents",
            ["agent_type", "binary", "host", "admin_state_up", "availability_zone"],
            registry=registry,
        )
        guageNeutronRouters = Gauge(
            "neutron_routers",
            "list of neutron Routers",
            ["router_name", "router_id", "project_id", "admin_state_up"],
            registry=registry,
        )
        guageNeutronFloatingips = Gauge(
            "neutron_floatingips",
            "list of neutron floating ips",
            ["network_name", "project", "project_id", "ip_type", "ip_status"],
            registry=registry,
        )
        guageNeutronNet_Size = Gauge(
            "neutron_net_size",
            "Neutron networks size",
            ["network_name"],
            registry=registry,
        )
        data = self.data
        if data:
            ips = self.get_floating_ips(data)
            ips.update(self.get_router_ips(data))
            for k, v in list(ips.items()):
                guageNeutronFloatingips.labels(*k).set(v)

            for agent in data["agents"]:
                guageNeutronAgents.labels(
                    agent["agent_type"],
                    agent["binary"],
                    agent["host"],
                    agent["admin_state_up"],
                    agent["availability_zone"],
                ).set(agent["alive"])
            for router_id, router_data in data["routers"].items():
                amount = True if router_data["status"] == "ACTIVE" else False
                guageNeutronRouters.labels(
                    router_data["name"],
                    router_id,
                    router_data["project_id"],
                    router_data["admin_state_up"],
                ).set(amount)

            for id, n in data["networks"].items():
                size = 0
                for subnet in n["subnets"]:
                    pools = self.data["subnets"][subnet]["pool"]
                    for pool in pools:
                        if ":" in pool["start"]:
                            # Skip IPv6 address pools; they are big enough to
                            # drown the IPv4 numbers we might care about.
                            continue
                        size += IPRange(pool["start"], pool["end"]).size
                label_values = [self.network_map(id)]
                guageNeutronNet_Size.labels(*label_values).set(size)

        return generate_latest(registry)


class Cinder(collectorBase):
    def _get_cinder_info(self):
        """Return a dict containing Cinder details."""
        info = {}
        try:
            info["agents"] = [
                {
                    "id": x.id,
                    "status": x.status,
                    "binary": x.binary,
                    "host": x.host,
                    "availability_zone": x["zone"],
                    "alive": x["state"],
                }
                for x in self.cloud.block_storage.services()
            ]
            info["volumes"] = {
                x.id: {
                    "name": x.name,
                    "project_id": x.location.project.id,
                    "status": x["status"],
                    "availability_zone": x["availability_zone"],
                    "volume_type": x["volume_type"],
                    "user_id": x["user_id"],
                    "bootable": x["bootable"],
                    "encrypted": x["encrypted"],
                    "size": x["size"],
                }
                for x in self.cloud.block_storage.volumes(all_projects=True)
            }

        except Exception as e:
            log.info("Error getting Cinder")
            log.error(e)

        return info

    def gen_data(self):
        data = self._get_cinder_info()
        return data

    def get_stats(self):
        registry = CollectorRegistry()
        guageCinderAgents = Gauge(
            "cinder_agents",
            "list of cinder agents",
            ["id", "status", "binary", "host", "availability_zone"],
            registry=registry,
        )
        guageCinderVolumes = Gauge(
            "cinder_volumes",
            "list of Cinder Volumes",
            [
                "id",
                "name",
                "project_id",
                "status",
                "availability_zone",
                "volume_type",
                "user_id",
                "bootable",
                "encrypted",
            ],
            registry=registry,
        )
        data = self.data
        if data:
            for agent in data["agents"]:
                amount = True if agent["alive"] == "up" else False
                guageCinderAgents.labels(
                    agent["id"],
                    agent["status"],
                    agent["binary"],
                    agent["host"],
                    agent["availability_zone"],
                ).set(amount)
            for volume_id, volume_data in data["volumes"].items():
                guageCinderVolumes.labels(
                    volume_id,
                    volume_data["name"],
                    volume_data["project_id"],
                    volume_data["status"],
                    volume_data["availability_zone"],
                    volume_data["volume_type"],
                    volume_data["user_id"],
                    volume_data["bootable"],
                    volume_data["encrypted"],
                ).set(volume_data["size"])
        return generate_latest(registry)


class AppMetrics(Thread):
    """
    Representation of Prometheus metrics and loop to fetch and transform
    application metrics into Prometheus metrics.
    """

    def __init__(self, cloud, refresh_interval):
        Thread.__init__(self)
        self.cloud_name = cloud
        self.daemon = True
        self.duration = 0
        self.refresh_interval = refresh_interval
        self.cache = ThreadSafeDict()
        self.osclients = []
        self.errors = 0

    def cache_me(self, osclient):
        self.osclients.append(osclient)
        log.debug(f"new {osclient.__class__.__name__} added to cache")

    def run(self):
        """Start collecting openstack resources details."""
        log.debug("Starting data gather thread")
        while True:
            start_time = time()
            errorsdict = []
            for osclient in self.osclients:
                try:
                    log.debug(f"Starting data gather on {osclient.get_cache_key()}")
                    self.cache[osclient.get_cache_key()] = osclient.gen_data()
                except Exception as e:
                    log.error(str(e))
                    log.error(
                        "failed to get data for cache key {}".format(
                            osclient.get_cache_key()
                        )
                    )
                    errorsdict.append(1)
                log.debug(f"Finished data gather on {osclient.get_cache_key()}")
            self.duration = time() - start_time
            if 1 in errorsdict:
                self.errors = 1
            else:
                self.errors = 0
            sleep(self.refresh_interval)

    def get_cache_data(self, key):
        if key in self.cache:
            return self.cache[key]
        else:
            return []

    def get_stats(self):
        registry = CollectorRegistry()
        labels = ["cloud"]
        label_values = [self.cloud_name]
        duration = Gauge(
            "openstack_exporter_cache_refresh_duration_seconds",
            "Cache refresh duration in seconds.",
            labels,
            registry=registry,
        )
        errors = Gauge(
            "openstack_exporter_collector_errors",
            "if a collect has problems getting data",
            labels,
            registry=registry,
        )

        duration.labels(*label_values).set(self.duration)
        errors.labels(*label_values).set(self.errors)
        return generate_latest(registry)

    def make_wsgi_app(self):
        """Create a WSGI app which serves the metrics from a registry."""

        def prometheus_app(environ, start_response):
            # Prepare parameters
            if environ["PATH_INFO"] == "/favicon.ico":
                # Serve empty response for browsers
                status = "200 OK"
                headers = [("", "")]
                output = b""
            else:
                # Bake output
                status = "200 OK"
                headers = [("Content-Type", CONTENT_TYPE_LATEST)]
                output = self.get_stats()
                for osclient in self.osclients:
                    output += osclient.get_stats()
            # Return output
            start_response(status, headers)
            return [output]

        return prometheus_app


def main():
    """Main entry point"""
    polling_interval_seconds = int(os.getenv("POLLING_INTERVAL_SECONDS", "250"))
    parser = argparse.ArgumentParser(description="Description of your program")
    parser.add_argument(
        "-c", "--cloud", help="Which cloud yaml name to use", required=False
    )
    parser.add_argument(
        "-p", "--port", help="Which port to use", default=9877, required=True
    )
    parser.add_argument(
        "-i",
        "--interval",
        help="seconds to poll the cloud",
        default=polling_interval_seconds,
    )
    parser.add_argument(
        "-l",
        "--log",
        dest="logLevel",
        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
        help="Set the logging level",
    )
    parser.add_argument("-s", "--stats", nargs="+", type=str)

    args = vars(parser.parse_args())
    if args["logLevel"]:
        log.setLevel(getattr(logging, args["logLevel"]))
    else:
        log.setLevel(logging.INFO)
    log.addHandler(logging.StreamHandler())

    cloud = openstack.connect()
    cloud.name = args["cloud"]

    log.info(f"Starting for {cloud.name}")
    app_metrics = AppMetrics(
        cloud=cloud.name,
        refresh_interval=int(args["interval"]),
    )

    class_map = {
        "placement": placement,
        "nova": nova,
        "neutron": Neutron,
        "cinder": Cinder,
        "glance": glance,
        "octavia": octavia,
        "instances": instances,
        "ironic": Ironic,
        "capacity_project": capacity_project,
        "capacity_host": capacity_host,
        "capacity_resources": capacity_resources,
    }

    # List of class names to load
    stats = args["stats"]

    # Load the classes from the map and add them to the cache
    for stat in stats:
        # Load the class from the map
        stat_class = class_map[stat]

        # Instantiate the class with the cloud and app_metrics objects
        stat_instance = stat_class(cloud, app_metrics)

        # Add the instance to the cache
        app_metrics.cache_me(stat_instance)

    # app_metrics.cache_me(placement_stats)
    # app_metrics.cache_me(nova_stats)
    # app_metrics.cache_me(instances_stats)
    # app_metrics.cache_me(glance_stats)
    # app_metrics.cache_me(octavia_stats)

    app = app_metrics.make_wsgi_app()
    httpd = make_server("", int(args["port"]), app)
    app_metrics.start()
    httpd.serve_forever()


if __name__ == "__main__":
    main()
