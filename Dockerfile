FROM python:3.8-slim

RUN apt-get update && apt-get upgrade -y && apt-get install python3-pip tini -y && apt-get clean

COPY ./requirements.txt /opt/requirements.txt
RUN pip install -U -r /opt/requirements.txt

COPY ./openstack-exporter.py /opt/openstack-exporter.py
ENTRYPOINT ["python3", "-u", "/opt/openstack-exporter.py"]
